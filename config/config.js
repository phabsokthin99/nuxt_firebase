
import { initializeApp } from 'firebase/app';
import {getFirestore} from 'firebase/firestore'
import {getAuth} from 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyADrKMwpZgAQeG9-SFy5EwiCzl9YMBAK8Q",
    authDomain: "javatoutoral.firebaseapp.com",
    projectId: "javatoutoral",
    storageBucket: "javatoutoral.appspot.com",
    messagingSenderId: "749256304700",
    appId: "1:749256304700:web:33543d4fe41badede7fb39"
  };


  const app = initializeApp(firebaseConfig);
  const projectFireStore = getFirestore(app)
  const projectAuth = getAuth(app)

  export {projectFireStore, projectAuth}