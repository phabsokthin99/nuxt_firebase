import { collection, onSnapshot, query } from "firebase/firestore"
import { projectFireStore } from "~/config/config"




const getCollection = (collectioName: any) => {

    const documents = ref(null)
    const collectionRef = query(collection(projectFireStore, collectioName))

    const unsuscript = onSnapshot(collectionRef, (qry) => {

        const results:any = []

        qry.forEach((doc) => {
            results.push({id: doc.id, ...doc.data()})
        })
        documents.value = results
    }, (err) => {
        console.log(err)
    })

    watchEffect((onInvalidate: any) => {
        onInvalidate(()=>unsuscript())
    })

    return {documents}
}

export default getCollection;