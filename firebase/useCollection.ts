
import { addDoc, collection, deleteDoc, doc } from "firebase/firestore";
import { projectFireStore } from "~/config/config";


const useCollection = (collectionName: any) => {

    const collectionRef = collection(projectFireStore, collectionName)
    const addDocs = async(formDoc: any) => {
        try{
            await addDoc(collectionRef, formDoc)
            return true;
        }
        catch(err){
            console.log(err)
        }
    }

    const deleteDocs = async(docID:any)=>{
        try{
            await deleteDoc(doc(collectionRef, docID))
            return true;
        }
        catch(err){
            console.log(err)
        }
    }

    return {addDocs,deleteDocs}
}

export default useCollection;